﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WSConvertisseur.Models
{
    public class Devise
    {
        public int Id { get; set; }

        public string NomDevise { get; set; }

        public double Taux { get; set; }

    }
}
