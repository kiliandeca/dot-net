﻿using ClientConvertisseurV1.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WSConvertisseur.Models;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ClientConvertisseurV1
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            ActionGetData();
        }

        private async void ActionGetData()
        {
            var result = await WSService.getInstance().GetAllDevisesAsync();
            this.selectorDevises.DataContext = new List<Devise>(result);
        }

        private async void clickToConvert(object sender, RoutedEventArgs e)
        {
            // Call app specific code to subscribe to the service. For example:
            string errorMsg = null;
            int montant;
            Devise selectedDevise = (Devise)this.selectorDevises.SelectedItem;
            bool isParsable = Int32.TryParse(this.montantInput.Text, out montant);
            if (selectedDevise == null)
            {
                errorMsg = "Il faut choisir une devise";
            }
            if (!isParsable)
            {
                errorMsg = "Il faut rentrer un montant valide (nombre entier)";
            }
            if (errorMsg != null)
            {
                MessageDialog msgError = new MessageDialog(errorMsg);
                await msgError.ShowAsync();
                return;
            }
            double montantCalculated = montant * selectedDevise.Taux;
            this.montantOutput.Text = montantCalculated.ToString();
            return;

        }




    }
}
