﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WSConvertisseur.Models;
using System.Net.Http.Headers;
using Windows.UI.Popups;

namespace ClientConvertisseurV1.Services
{
    class WSService
    {
        static HttpClient client = new HttpClient();

        static WSService instance = new WSService();

        public WSService()
        {
            client.BaseAddress = new Uri("http://localhost:5000/api/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Devise>> GetAllDevisesAsync()
        {
            try
            {

                List<Devise> devises = new List<Devise>();
                HttpResponseMessage response = await client.GetAsync("devise");
                if (response.IsSuccessStatusCode)
                {
                    devises = await response.Content.ReadAsAsync<List<Devise>>();
                }
                return devises;
            }
            catch (HttpRequestException e)
            {

                await new MessageDialog("HTTP Exception "+e.Message).ShowAsync();

                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }

        }

        static public WSService getInstance()
        {
            return WSService.instance;
        }

    }
}
