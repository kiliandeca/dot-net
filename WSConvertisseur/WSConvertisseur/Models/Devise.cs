﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WSConvertisseur.Models
{
    public class Devise
    {
        [Required]
        public int Id { get; set; }

        public string NomDevise { get; set; }

        public double Taux { get; set; }

        public Devise()
        {

        }

        public Devise(int Id, string NomDevise, double Taux)
        {
            this.Id = Id;
            this.NomDevise = NomDevise;
            this.Taux = Taux;
        }
    }
}
