﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WSConvertisseur.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WSConvertisseur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviseController : ControllerBase
    {

        List<Devise> listeDevise = new List<Devise>();

        public DeviseController()
        {
            this.listeDevise.Add(new Devise(1, "Dollar", 1.08));
            this.listeDevise.Add(new Devise(2, "Franc Suisse", 1.07));
            this.listeDevise.Add(new Devise(3, "Yen", 120));
        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns>Http response</returns>
        /// <response code="200">When the currency list exists</response>
        /// <response code="404">When the currency list doesn't exist</response>
        // GET: api/<DeviseController>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Devise>), 200)]
        public IEnumerable<Devise> GetAll()
        {
            return this.listeDevise.AsQueryable();
        }

        /// <summary>
        /// Get currency by ID
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="ID">Currency ID</param>
        /// <response code="200">When currecy is found</response>
        /// <response code="404">When currency not found</response>
        // GET api/<DeviseController>/5
        [HttpGet("{id}", Name = "GetDevise")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Devise), 200)]
        [ProducesResponseType(404)]
        public IActionResult GetById(int id)
        {
            Devise devise = listeDevise.FirstOrDefault(d => d.Id == id);
            if (devise == null)
            {
                return NotFound();
            }
            return Ok(devise);
        }
        
        /// <summary>
        /// Add new currency
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="Devise">JSON object of Devise</param>
        /// <response code="201">When currecy is added</response>
        /// <response code="400">When fail to add currency</response>
        // POST api/<DeviseController>
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Devise), 201)]
        [ProducesResponseType(404)]
        public IActionResult Post([FromBody] Devise devise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            listeDevise.Add(devise);
            return CreatedAtRoute("GetDevise", new { id = devise.Id }, devise);
        }

        /// <summary>
        /// Modify currency by ID
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="Devise">JSON object of Devise</param>
        /// <response code="200">When Devise is updated</response>
        /// <response code="400">When the update returns an error</response>
        /// <response code="404">When the update returns an error</response>
        // PUT api/<DeviseController>/5
        [HttpPut("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Devise), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]

        public IActionResult Put(int id, [FromBody] Devise devise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != devise.Id)
            {
                return BadRequest();
            }
            int index = listeDevise.FindIndex(d => d.Id == id);
            if (index < 0)
            {
                return NotFound();
            }
            listeDevise[index] = devise;
            return NoContent();
        }

        /// <summary>
        /// Delete a currency
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="ID">ID of Devise to delete</param>
        /// <response code="200">When Devise is deleted</response>
        /// <response code="404">When the deletion returns an error</response>
        // DELETE api/<DeviseController>/5
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Delete(int id)
        {
            Devise devise = listeDevise.FirstOrDefault(d => d.Id == id);
            if (devise == null)
            {
                return NotFound();
            }
            listeDevise.Remove(devise);
            return Ok(devise);
        }
    }
}
