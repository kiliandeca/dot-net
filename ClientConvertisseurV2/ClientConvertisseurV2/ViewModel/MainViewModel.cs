﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using ClientConvertisseurV2.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using ClientConvertisseurV2.Services;
using System.Windows.Input;
using Windows.UI.Popups;

namespace ClientConvertisseurV2.ViewModel
{
    public class MainViewModel : ViewModelBase
    {

        private ObservableCollection<Devise> _comboBoxDevises;
        public ObservableCollection<Devise> ComboBoxDevises
        {
            get { return _comboBoxDevises; }
            set
            {
                _comboBoxDevises = value;
                RaisePropertyChanged();// Pour notifier de la modification de ses données
            }
        }

        private string _montantEuros;
        public string MontantEuros
        {
            get { return _montantEuros; }
            set
            {
                _montantEuros = value;
                RaisePropertyChanged();
            }
        }

        private Devise _comboBoxDeviseItem;

        public Devise ComboBoxDeviseItem
        {
            get { return _comboBoxDeviseItem; }
            set { 
                _comboBoxDeviseItem = value;
                MontantOutput = "";
            }
        }

        private string _montantOutput;

        public string MontantOutput
        {
            get { return _montantOutput; }
            set { 
                _montantOutput = value; 
                RaisePropertyChanged();
            }
        }


        public MainViewModel()

        {
            ActionGetData();
            BtnSetConversion = new RelayCommand(ActionSetConversion);
        }
        private async void ActionGetData()
        {
            var result = await WSService.getInstance().GetAllDevisesAsync();
            this.ComboBoxDevises = new ObservableCollection<Devise>(result);
        }

        private async void ActionSetConversion()
        {

            try
            {
                double montant = Double.Parse(this.MontantEuros);
                double outputMontant = montant * this.ComboBoxDeviseItem.Taux;
                this.MontantOutput = outputMontant.ToString();
            }
            catch (ArgumentNullException e)
            {
                MessageDialog msgError = new MessageDialog("Il faut indiquer un montant ! ");
                await msgError.ShowAsync();
            }
            catch (FormatException e)
            {
                MessageDialog msgError = new MessageDialog("Il faut renseigner un montant et non une chaine de charactères");
                await msgError.ShowAsync();
            }
            catch (NullReferenceException e)
            {
                MessageDialog msgError = new MessageDialog("Il faut renseigner une devise");
                await msgError.ShowAsync();
            }
            catch (Exception e)
            {
                MessageDialog msgError = new MessageDialog("Il faut renseigner une exception");
                await msgError.ShowAsync();

            }

        }



        public ICommand BtnSetConversion { get; private set; }
    }
}
